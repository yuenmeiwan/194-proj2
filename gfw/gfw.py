# NOTE: The following iptables rule must be inplace or else the OS will
# insert unwanted RST packets:
# iptables -A OUTPUT -p tcp --tcp-flags RST RST -j DROP


import sys, threading, ipaddress, json, time
from scapy.all import *

################### Network Helper Functions ###################

# This sniffer thread captures the ip of the responding server (used for ICMP),
# and stops when a RST or HTTP packet is received.
class GFW_Sniffer(threading.Thread):
    def __init__(self, dst, timeout=10):
        super(GFW_Sniffer, self).__init__()
        self.dst = dst
        self.timeout = timeout
        self.is_rst = False
	self.src_ip = ""

    def _is_rst(self, pkt):
	self.src_ip = pkt[IP].src
        if 'TCP' in pkt and pkt['TCP'].flags & 0x04:
            self.is_rst = True
        return self.is_rst

    def sniffer(self, pkt):
        self.src_ip = pkt[IP].src

    def run(self):
        sniff(prn=self.sniffer, timeout=self.timeout, store=0, filter="src %s or icmp" % self.dst, stop_filter=lambda x: self._is_rst(x) or 'HTTP' in x)

# Poke sends a specially crafted series of packets that look like
# a valid TCP session to the GFW
def poke(china_host, http_str, ttl):
   syn = IP(dst=china_host, ttl=ttl) / TCP(dport=80, flags='S', sport=8000, seq=1)
   ack = IP(dst=china_host, ttl=ttl) / TCP(dport=80, sport=8000,
                ack=2, flags='A')
   http_req = IP(dst=china_host, ttl=ttl) / TCP(dport=80, sport=8000, flags='A') / http_str
   send(syn, verbose=False)
   send(ack, verbose=False)
   send(http_req, verbose=False)

# pokeN repeatedly pokes the GFW, and returns whether or not the GFW responded,
# and the IP of the responding server.
TRY_TIMES=5
def pokeN(china_host, http, ttl=64):
    sniffer = GFW_Sniffer(china_host)
    sniffer.start()

    for _ in range(TRY_TIMES):
        # Sleep a bit so that the responses from different pokes don't interfere with each other
        time.sleep(1)

	poke(china_host, http, ttl)

    sniffer.join()
    if sniffer.is_rst:
        return True, sniffer.src_ip

    # If the src_ip doesn't match, that means we got a ICMP reply.
    # Retry poking the appropriate IP so that filters are correct.
    if sniffer.src_ip != china_host and sniffer.src_ip != "":
	return pokeN(sniffer.src_ip, http, ttl)[0], sniffer.src_ip

    return False, sniffer.src_ip


################### CLI Entrypoints ###################

TRIGGER_QUERY="GET /?falun HTTP/1.1\r\nHost: www.google.com\r\n\r\n"

def gfw_traceroute(ip):
     print("Output format:")
     print("Hop <NUMBER> : <IP> : <IF_FIREWALL_PRESENT>")
     for i in range(1, 64):
	is_blocked, res_ip = pokeN(ip, TRIGGER_QUERY, i)
	print("Hop %d : %s : %s" % (i, res_ip, is_blocked))
	if res_ip == ip:
 	    break

# XXX: Handle running out of IPs
# XXX: Parallelize this..
def minstring(http_str, china_ips_path):
    china_ips = None
    with open(china_ips_path) as china_ips_file:    
        china_ips = json.load(china_ips_file)

    china_ip_index = 0
    if not pokeN(china_ips[china_ip_index], http_str)[0]:
	print("Invalid http string to minify: %s" % http_str)
	return
    # We used that IP for verifying the http_str works
    china_ip_index += 1

    # Keep on trying to add asteriks until we don't add an asterik
    # for an entire loop through modded_str
    modded_str = http_str
    had_change = True
    while had_change:
	had_change = False
        for i in range(len(modded_str)):
	    if modded_str[i] == "*":
		continue
            test_str = modded_str[:i] + "*" + modded_str[i+1:]
	    print("Trying " + test_str)
            if pokeN(china_ips[china_ip_index], test_str)[0]:
                china_ip_index += 1
                modded_str = test_str
                had_change = True
		break

    # Keep on looping through the modded string to try and delete
    # asteriks until there's nothing that we can delete to make it shorter.
    had_change = True
    while had_change:
	had_change = False
        for i in range(len(modded_str)):
	    if modded_str[i] == "*":
                test_str = modded_str[:i] + modded_str[i+1:]
		print("Trying " + test_str)
                if pokeN(china_ips[china_ip_index], test_str)[0]:
                    china_ip_index += 1
                    modded_str = test_str
                    had_change = True
		    break
    
    print("http string reduced to: %s" % modded_str)

def liverange(subnet, outfile_prefix):
    u_subnet = subnet.decode('utf-8')
    hosts = list(ipaddress.ip_network(u_subnet).hosts())

    live_hosts = []
    for i, _host in enumerate(hosts):
        host = str(_host)
        if is_broadcast(host):
            continue
        if not pokeN(host, "")[0] and pokeN(host, TRIGGER_QUERY)[0]:
            live_hosts.append(host)
	    print("%s is live" % host)
        else:
	    print("%s is not live" % host)

        if i != 0 and i % 255 == 0:
            dump_json(live_hosts, outfile_prefix + "-" + host)

    dump_json(live_hosts, outfile_prefix)

################### Liverange Helpers ###################

def is_broadcast(host):
    return host.split(".")[3] == "255"

def dump_json(to_dump, path):
    with open(path, 'w') as outfile:
        json.dump(to_dump, outfile)
 

TSINGHUA_IP = "166.111.4.100"
def detect(ip):
    is_blocked, _ = pokeN(TSINGHUA_IP, "GET /?falun HTTP/1.1\r\nHost: " + ip + "\r\n\r\n")
    print("{0} is {1}blocked.".format(ip, "" if is_blocked else "not "))

def print_usage():
    print('''Usage: {0} detect <IP>
{0} traceroute <IP>
{0} liverange <CIDR_BLOCK> <OUTFILE_PREFIX>
{0} minstring <STRING> <CHINA_IPS>'''.format(sys.argv[0]))
    exit()

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print_usage()

    if sys.argv[1] == 'detect':
        detect(sys.argv[2])
    elif sys.argv[1] == 'traceroute':
        gfw_traceroute(sys.argv[2])
    elif sys.argv[1] == 'liverange':
        liverange(sys.argv[2], sys.argv[3])
    elif sys.argv[1] == 'minstring':
        #minstring(sys.argv[2], sys.argv[3])
        minstring(TRIGGER_QUERY, sys.argv[3])
    else:
        print_usage()
