
## A module for extracting various bits of "content derived metadata"
## from HTTP and other connections.

module Metadata;

export {
	redef enum Log::ID += { LOG, LOG2, LOG3 };


	# A record for how to extract usernames
	# from sites.
	type MetadataRule: record {
		# The hostname used to check the cookies
		hostname:pattern;

		# The cookies to trigger on
		cookie: pattern;

		# The start and end which brackets the user name
		userextractstart: pattern;
		userextractend: pattern;

	        # Cookiedomain, the generic name
	        # which the cookie goes into,
	        # used to make the logging better.
	        cookiedomain: string;
		};


	# A record associated with an HTTP flow
	# track various pieces of metadata as they are built up
	# to extract user ID from HTTP
	type MetadataInfo: record {
		# connection unique identifier
		uid:      string &log;

		# Hostname from the HTTP connection
		hostname: string &log &optional;
		cookiedomain: string &log &optional;
		username: string &log &optional;
		cookies:  string &optional;
		targetcookie: string &log &optional;

		# Should this be analyzed to find out a
		# username
		analyze:  bool   &log &default=F;

		# Do we now know the username?
		analyzed: bool   &log &default=F;

		# The rule for HTTP analysis to extract
		# usernames
		rules:    MetadataRule &optional;

		# The last piece of data sent over an
		# HTTP connection
		lastchunk: string &optional;
		};


	# A record to say when a cookie has been
	# seen on a connection.
	type CookieActivity: record{
	        cookie: string &log;
		active: bool &log &default=T;
		ip: addr &log;
		ua: string &log &optional;
		start: time &log;
		end: time &log &optional;
	};


        # A record to say multiple cookies have the same association
	type CookieLinking: record{
	     ip: addr &log;
	     ua: string &log &optional;
	     cookies: set[string] &log;
	};


        # A type declaration for metadata rule types below
	type MetadataRuleVector : vector of MetadataRule;

	# The sites which are supported.
        # This needs to be directly included in the code because
        # bro doesn't allow loading regular expressions...
	global MetadataRules  = MetadataRuleVector(
			    [$hostname = /(.*\.)?arstechnica.com/,
			     $cookie = /phpbb3_5qbzr_u=.*/,
			     $userextractstart = /<span class=\"welcome\">/,
			     $userextractend = /</,
			     $cookiedomain = "arstechnica"],
			    [$hostname = /(.*\.)?*.yahoo.com/,
			     $cookie = /B=.*/,
			     $userextractstart = /<span class=\"welcome\">/,
			     $userextractend = /</,
			     $cookiedomain = "yahoo"],
			    [$hostname = /(.*\.)?amazon.com/,
			     $cookie = /ubid-main=.*/,
			     $userextractstart = /\"Hello, /,
			     $userextractend = /\"/,
			     $cookiedomain = "amazon"]
			    ) &redef;



	# Log functions
	global log_metadata: event(rec: MetadataInfo);
	global log_cookie: event(rec: CookieActivity);
	global log_cookie2: event(rec: CookieLinking);

	# Defn of event for extracting HTTP header info
	global title_chunk: event(f: fa_file, data: string);

	# Defn of event for checking metadata for user tracking
	global check_metadata: function (c: connection);
	}


# A global table mapping (hostname, cookie) to user
# The expiration is deliberately high.  If someone is already
# in this table, we don't bother looking to it.
global UserTable : table[string, string] of string &read_expire=10day;


# Add a metausername tag to HTTP, and log it
redef record HTTP::Info += {
	# The username and cookie for this connection
	metausername: string &optional &log;
	metacookie: string &optional &log;

	# Extracted HTML title.
	htmltitle: string &optional &log;

	# ALL cookies requested on the connection
	cookies: string &optional &log;

	# And the metadata tracker.
	metadata: MetadataInfo &optional;

	# A small record of previous data for HTTP tile extraction
	# This is optional and NOT logged.
	httpchunkcache: string &optional;
};






# HTTP title extraction:
# If the file is 'text/html', add the analyzer
# to see if you can extract the text between "<title></title>"
event file_sniff(f: fa_file, meta: fa_metadata){
        if (f?$http && meta?$mime_type &&
                meta$mime_type == "text/html"){
           f$http$httpchunkcache = "";
           Files::add_analyzer(f,
              Files::ANALYZER_DATA_EVENT,
              [$stream_event=title_chunk]);
        }
}


# The event for extracting HTTP title info itelf.
# How Bro handles files is it raises an event in order for each
# hunk of data, you need to potentially merge the pieces which
# is why there is the httpchunkcache added to the HTTP record.
event title_chunk(f: fa_file, data: string){

	f$http$httpchunkcache = f$http$httpchunkcache + data;


	# HERE is some area you need to write some code...
	# XXX: Do case insensitive better.
	# XXX: Strip whitespace.
    local title_regex = /<[Tt][Ii][Tt][Ll][Ee]>.*<\/[Tt][Ii][Tt][Ll][Ee]>/;
    if (title_regex in data) {
        for (res in find_all(data, title_regex)) {
            local title = str_split(res, vector(7, |res|-8))[2];
            f$http$htmltitle = title;
        }
    }

        # You should check for text between <TITLE>.*</TITLE> and
        # extract it.  Make the regular expression case insensitive
        # and strip out whitespace [ \n\t\r] in getting the title.


	# And the final analysis: IF we've gotten more than 2048B on
	# the connection, we should stop analyzing further data
        if(|f$http$httpchunkcache| > 2048){
		Files::remove_analyzer(f, Files::ANALYZER_DATA_EVENT,
					 [$stream_event=title_chunk]);
	        return;
        }
}




# Cookie framework to check for tracking cookies,
# this loads from the metacookies.txt file
type Idx: record { domain: string;};
type MetacookieInfo: record { cookie: string; name: string;};
global check_cookies: table[string] of MetacookieInfo = table();


# An RE for splitting cookies
global cookie_re: pattern = /(; )|(&)|(\|)|(:)/;


# The Bro initialization functions for this file.
event bro_init(){

	# Create the logging streams
	Log::create_stream(Metadata::LOG,
                           [$columns=MetadataInfo,
		            $ev=log_metadata]);
	Log::create_stream(Metadata::LOG2,
                           [$columns=CookieActivity,
		            $ev=log_cookie]);
	Log::create_stream(Metadata::LOG3,
                           [$columns=CookieLinking,
		            $ev=log_cookie2]);

	# Load the check_cookies table
	local str = fmt("%s/metacookies.txt", @DIR);
	Input::add_table([$source=str,
			$name="cookies",
		     	$idx=Idx, $val=MetacookieInfo,
			$destination=check_cookies]);
	Input::remove("cookies");
}


# A function to see if a domain/cookie pair has a
# mapped user-name in the UserTable already...
function check_cookie(d: string, c: string) : string
	{
	if([d, c] in UserTable) return UserTable[d, c];
	for (t in split_string(d, /\./)){
		if(|split_string1(d,/\./)| == 1) return "";
		d = split_string1(d,/\./)[1];
		if([d, c] in UserTable) return UserTable[d, c];
        }
	return "";
	}

# A function to add the metadata annotation
# to a connection if it doesn't yet exist, othrwise
# it is a no-op
function set_state(c: connection)
	{
	if (! c?$http){
		return;
	}
	if (! c$http?$metadata){
		local s: MetadataInfo;
		c$http$metadata = s;
		c$http$metadata$uid = c$uid;
	}
}

# For a connection, check to see if its time to analyze
# the HTTP cookie/username metadata.
function check_metadata(c: connection)
	{

	# Can't try until we have both hostname and usernames
	if (!c$http$metadata?$cookies)
		return;
	if (!c$http$metadata?$hostname)
		return;

	# HERE you need to do what's needed to set
	# metdata$analyze if you need to, because
	# you need to find the username associated
	# with a cookie, by checking to see if a rule
	# specifies you should analyze the response.
	# You should also check to see if the cookie is
	# already known and, if so, you should just set the
	# right record in the HTTP information structure.

	# XXX: Check if the cookie is already known.
	# Loop over all our metadata rules.
	for (i in MetadataRules) {
	    local rule=MetadataRules[i];
	    # Check if the connection's metadata matches a rule
        if (rule$cookie in c$http$metadata$cookies && rule$hostname in c$http$metadata$hostname) {
            if (c$http$metadata?$username) {
                c$http$metadata$analyze = F;
            } else {
                c$http$metadata$analyze = T;
            }
            # Save the matching cookie.
            for (tc_step1 in find_all(c$http$metadata$cookies, rule$cookie)) {
                c$http$metadata$targetcookie = split_string(tc_step1, cookie_re)[0];
            }
            c$http$metadata$cookiedomain = rule$cookiedomain;
            c$http$metadata$rules = rule;
        }
	}
}


# I want to make sure that the base HTTP stuff gets a chance to run
# first but that's priority 5.  But other stuff should run after this
event http_header(c: connection, is_orig: bool, name: string, value: string) &priority=4
	{
	# This function should check the host and cookie headers
	# in HTTP requests to set the $metadata fields, and then
	# call check_metadata()
	if ( is_orig ){
		set_state(c);
		if (name == "HOST"){
			local h: string;
			h = split_string1(value, /:/)[0];
			c$http$metadata$hostname = h;
			check_metadata(c);
		}
      		if (name == "COOKIE"){
			c$http$metadata$cookies = value;
			c$http$cookies = value;
			check_metadata(c);
			}
      		}
	}


# If we need to log the metadata, we log it...
event connection_state_remove(c: connection)
	{
	if ( c?$http && c$http?$metadata && (c$http$metadata?$username
		             || c$http$metadata$analyze)){
		Log::write(Metadata::LOG, c$http$metadata);
		}

	}


# The actual analyzer to run over HTTP replies to look
# for the username
event metadata_chunk(f: fa_file, data: string){
	# HERE you should keep track of the current
	# chunk of data you get and the previous chunk,
	# and see if the rule on the metadata record
    # can extract the username.

	# If so, you should set the username, and also
	# set the UserTable for the name/cookie pair.

    # Keep track of this and the previous chunk of data.
    local chunk = f$http$metadata$lastchunk + data;
    f$http$metadata$lastchunk = data;
    
    # Try to find the beginning of the username.
    local step1 = split_string1(chunk, f$http$metadata$rules$userextractstart);
    if (|step1| == 1) {
        return;
    }

    # Try to find the end of the username.
    local step2 = split_string1(step1[1], f$http$metadata$rules$userextractend);
    if (|step2| == 1) {
        return;
    }

    # Save the resulting username.
    local username = step2[0];
    f$http$metadata$username = username;
    f$http$metadata$analyzed = T;
    f$http$metadata$analyze = F;
    Files::remove_analyzer(f, Files::ANALYZER_DATA_EVENT, [$stream_event=metadata_chunk]);
    UserTable[f$http$metadata$hostname, f$http$metadata$targetcookie] = username;
}

# User identification extraction,
# does it need to run?
event file_sniff(f: fa_file, meta: fa_metadata){
	if (f?$http && f$http?$metadata && f$http$metadata$analyze
		&& !f$http$metadata$analyzed){
	        # HERE you should check if the cookie has
	        # been picked up already and, if not,
	        # check if the mimetype is text/html and if so,
	        # activate the metadata_chunk analyzer on the file
	    if ([f$http$metadata$hostname, f$http$metadata$targetcookie] in UserTable) {
            f$http$metadata$username = UserTable[f$http$metadata$hostname, f$http$metadata$targetcookie];
            f$http$metadata$analyze = F;
            f$http$metadata$analyzed = T;
            return;
	    }
        if (f?$http && meta?$mime_type &&
                meta$mime_type == "text/html"){
            f$http$metadata$lastchunk = "";
           Files::add_analyzer(f,
              Files::ANALYZER_DATA_EVENT,
              [$stream_event=metadata_chunk]);
        }
    }
}








# User cookie tracking, looking only at requests
# Get the domain of a cookie
function get_cookie_domain(d: string) : string{
	if(d in check_cookies) return d;
	for(t in split_string(d, /\./)){
	      local tmp = split_string1(d, /\./);
	      if(|tmp| > 1) {
	      	      d = split_string1(d, /\./)[1];
	     	      if(d in check_cookies) return d;
	      }
	}
	return "";
}

# Report cookie activity.
function reportActive(t:table[string, addr, string] of CookieActivity,
	idx: any): interval
	{
	local a: string;
	local b: addr;
	local c: string;
	[a,b,c] = idx;
	Log::write(Metadata::LOG2, t[a,b,c]);
	return 0sec;
	}

# Activity is cookie, ip, user-agent,
# On deletion, triggers the log.
global ActiveCookieTable : table [string, addr, string] of
       CookieActivity &read_expire=10min &expire_func=reportActive;

function reportLinkage(t:table[addr, string, string] of CookieLinking,
	idx: any): interval
	{
	local a: addr;
	local b: string;
	local c: string;
	[a,b,c] = idx;
	if(|t[a,b,c]$cookies| > 1){
		Log::write(Metadata::LOG3, t[a,b,c]);
		}
	return 0sec;
	}


# Index is address, referrer, user-agent
global LinkedCookieTable : table [addr, string, string] of
       CookieLinking &read_expire=1min &expire_func=reportLinkage;

# Do the actual cookie linking processing
event http_message_done (c: connection, is_orig: bool,
      stat: http_message_stat) &priority=4 {
      if (is_orig && c?$http && c$http?$host && c$http?$cookies){
      	 local domain = get_cookie_domain(c$http$host);
	 if(domain == "") return;
	 local cookies = split_string(c$http$metadata$cookies, cookie_re);
	 for(t in cookies){
	       local cookiename = split_string1(cookies[t], /=/)[0];
	       if(cookiename == check_cookies[domain]$cookie){

		  # HERE you should check and set the ActiveCookieTable
		  # for cookies that should be tracked.
		  # You should also set the http$metacookie field
		  # for tracking purposes
		        if (c?$id && c$id?$orig_h && c$http?$user_agent) {
		            local track : CookieActivity = [$cookie=cookiename, $ip=c$http$id$orig_h, $ua=c$http$user_agent, $start=network_time()];
                    ActiveCookieTable[cookiename, c$http$id$orig_h, c$http$user_agent] = track;
                    Log::write(Metadata::LOG2, ActiveCookieTable[cookiename, c$http$id$orig_h, c$http$user_agent]);
                    c$http$metacookie = cookiename;

		 # and HERE you should also process cookie linking:
		 # two different tracked cookies with the same
		 # referrer and IP should be tracked in the LinkedCookieTable.
		 if (c$http?$referrer) {
                if ([c$id$orig_h, c$http$referrer, c$http$user_agent] in LinkedCookieTable) {
		        add LinkedCookieTable[c$id$orig_h, c$http$referrer, c$http$user_agent]$cookies[cookiename];
		        } else {
		            local linking : CookieLinking = [$ip=c$id$orig_h, $ua=c$http$user_agent, $cookies=set(cookiename)];
                    LinkedCookieTable[c$id$orig_h, c$http$referrer, c$http$user_agent] = linking;
		        }
	       }
	 }
	 }
	 }
      }
}
